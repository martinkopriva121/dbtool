﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics.PerformanceData;
using MySql.Data.MySqlClient;
using System.Threading;

namespace DatabaseTool
{
    public partial class Form1 : Form
    {
        private MySqlConnection connection;
        private string QId = "a ", QTitle = "b ", QMethod = " c", QFlags = "d ";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button2.Hide();
            tabControl1.Hide();
            menuStrip1.Hide();
        }

        private void DatabaseConnect()
        {
            if (textBox1.Text == "Username" || textBox2.Text == "Password" || textBox3.Text == "Server" || textBox4.Text == "Database")
                return;

            string Username = textBox1.Text;
            string Password = textBox2.Text;
            string Server = textBox3.Text;
            string Database = textBox4.Text;
            string connectionString;
            connectionString = "SERVER=" + Server + ";" + "DATABASE=" + Database + ";" + "UID=" + Username + ";" + "PASSWORD=" + Password + ";";

            connection = new MySqlConnection(connectionString);
        }

        private void OpenConnection()
        {
            try
            {
                connection.Open();
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect");
                        break;
                    case 1045:
                        MessageBox.Show("Invalid Username or Password");
                        break;
                    default:
                        break;
                }
            }
        }

        private bool Disconnect()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread conn = new Thread(DatabaseConnect);
            conn.Start();
            conn.Join();

            if (connection.State == ConnectionState.Open)
                return;
            else
            {
                StartConnectionThread();
                ShowApplicationMenu();
            }
        }

        private void ResizeWindow(int WidthSize, int HeightSize)
        {
            if (WidthSize <= 0 || HeightSize <= 0)
                return;
            else
                this.Size = new System.Drawing.Size(WidthSize, HeightSize);
        }

        private void ShowApplicationMenu()
        {
            // Hide login objects
            textBox1.Hide();
            textBox2.Hide();
            textBox3.Hide();
            textBox4.Hide();
            button1.Hide();
            // Resize window
            ResizeWindow(800, 600);
            // Show core of app
            tabControl1.Show();
            menuStrip1.Show();
        }

        private void HandleReconnectFunction()
        {
            ResizeWindow(300, 300);
            tabControl1.Hide();
            menuStrip1.Hide();
            textBox1.Show();
            textBox2.Show();
            textBox3.Show();
            textBox4.Show();
            button1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (connection.State == ConnectionState.Open)
            {
                Disconnect();
                HandleReconnectFunction();
            }
            else
                return;
        }

        private bool IsTextChanged(int TextBoxId)
        {
            switch (TextBoxId)
            {
                case 5: if (!String.IsNullOrEmpty(textBox5.Text)) return true; break;
                case 6: if (!String.IsNullOrEmpty(textBox6.Text)) return true; break;
                case 7: if (!String.IsNullOrEmpty(textBox7.Text)) return true; break;
            }
            return false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!IsTextChanged(5) && !IsTextChanged(6) && !IsTextChanged(7))
                return;

            if (connection.State == ConnectionState.Closed)
                StartConnectionThread();

            List<string>[] list;
            list = SelectFromQuestTemplate();

            dataGridView1.Rows.Clear();

            for (int i = 0; i < list[0].Count; ++i)
            {
                int number = dataGridView1.Rows.Add();
                dataGridView1.Rows[number].Cells[0].Value = list[0][i];
                dataGridView1.Rows[number].Cells[1].Value = list[1][i];
                dataGridView1.Rows[number].Cells[2].Value = list[2][i];
                dataGridView1.Rows[number].Cells[3].Value = list[3][i];
            }
        }
        public List<string>[] SelectFromQuestTemplate()
        {
        
            string query = " ";

            if (IsTextChanged(6))
                query = "SELECT Id, Title, Flags, Method FROM quest_template WHERE Title LIKE '%" + textBox6.Text.ToString() + "%';";
            else if (IsTextChanged(5))
                query = "SELECT Id, Title, Flags, Method FROM quest_template WHERE Id=" + textBox5.Text.ToString() + ";";
            else if (IsTextChanged(7))
                query = "SELECT Id, Title, Flags, Method FROM quest_template WHERE Flags =" + textBox7.Text.ToString() + ";";
            else
            {
                MessageBox.Show("Query Error");
                return null;
            }

            List<string>[] list = new List<string>[4];
            list[0] = new List<string>();
            list[1] = new List<string>();
            list[2] = new List<string>();
            list[3] = new List<string>();

            if (connection.State == ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    list[0].Add(dataReader["id"] + "");
                    list[1].Add(dataReader["Title"] + "");
                    list[2].Add(dataReader["Flags"] + "");
                    list[3].Add(dataReader["Method"] + "");
                }

                dataReader.Close();
                connection.Close();

                return list;
            }
            else
            {
                MessageBox.Show("Query Error");
                return list;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
            MessageBox.Show("Thank for Use");
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("(c) Jinia Trinity Core database viewer");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void StartConnectionThread()
        {
            Thread t = new Thread(OpenConnection);
            t.Start();
            t.Join();
        }
    }
}
